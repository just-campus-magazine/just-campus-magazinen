const Category = require('../model/Category')
const mongoose = require('mongoose')


exports.getAllCategory = async(req, res, next) => {
    try {
        let categories = await Category.find()
        console.log(categories)
        return res.render('pages/admin/category', { title: 'Admin', user: req.user, categories })
    } catch (e) {
        console.log(e)
        next(e)
    }
}


exports.createCategoryPage = async(req, res, next) => {
    try {
        return res.render('pages/admin/create_category', { title: 'Admin', user: req.user })
    } catch (e) {
        console.log(e)
        next(e)
    }
}

exports.createCategory = async(req, res, next) => {
    try {

        let { category } = req.body
        let createdCategory = await Category.create({
            name: category
        })

        return res.redirect('/admin/category')
            //console.log(createdCategory)

    } catch (e) {
        console.log(e)
        next(e)
    }
}



exports.deleteCategory = async(req, res, next) => {
    try {
        let category_id = req.params.id

        if (!mongoose.Types.ObjectId.isValid(category_id)) {
            return res.json({
                msg: 'There is no Category with this id'
            })
        }

        let deletedCategory = await Category.findByIdAndDelete(category_id)

        if (deletedCategory) {
            return res.redirect('/admin/category')
        }


    } catch (e) {
        console.log(e)
        next(e)
    }
}