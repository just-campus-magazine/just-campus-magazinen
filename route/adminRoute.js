const router = require('express').Router()
const { getAllCategory, createCategory, createCategoryPage, deleteCategory } = require('../controller/adminController')
const { authCheck } = require('../middleware/authCheck')


router.get('/category', authCheck, getAllCategory)
router.get('/create-category', authCheck, createCategoryPage)
router.post('/create-category', authCheck, createCategory)
router.get('/delete-category/:id', authCheck, deleteCategory)

module.exports = router